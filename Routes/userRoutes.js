const express = require('express');
const router = express.Router();

const userController = require('../Controllers/useController.js');

//[ROUTES]
// this route is respnsible of the registration of the user.
router.post('/register', userController.userRegistration);

router.post('/login', userController.userAuthentication);

router.post('/details', userController.getProfile);

module.exports = router;

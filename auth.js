const jwt = require('jsonwebtoken');
// user  defined string data that will be used to create
// JSON web tokens
// Used in the algo for ecrypting our data which makes it
// difficult to decode the information without defined secret keyword.

const secret = 'CourseBookingAPI';

//[Section] JSON web token
// Json web token or jwt is a way of securely passing the
// server to the frontend or the other parts of the server.

// Information is kept secrute through the use of the secret code.
// only the system will know the secret code. that can
// decode the encrypted information.

//Token Creation

/*
  Analoy: Pack the gift/information and provide the secret code for the key
 */

//  the argum,ent that will be passed to our paramenter(user) will be the
// document/informantion of our user

module.exports.createAccessToken = (user) => {
  // will contain the data that will be passed to other parts of ou API
  const data = {
    _id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  //.sign() from jwt package will generate a JSOn web token using the jwt's sign method
  // syntax jwd.sign(payload,secretload, options)

  return jwt.sign(data, secret);
};
